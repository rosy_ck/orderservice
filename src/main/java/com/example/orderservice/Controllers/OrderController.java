package com.example.orderservice.Controllers;
import com.example.orderservice.CustomInterface.OrderTransaction;
import com.example.orderservice.Entities.OrderDetail;
import com.example.orderservice.Entities.Supply;
import com.example.orderservice.RabbitMQ.Sender;
import com.example.orderservice.Repositories.ElementCartRepository;
import com.example.orderservice.Repositories.SupplyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.example.orderservice.Entities.ElementCart;
import com.example.orderservice.Entities.ShopOrder;
import com.example.orderservice.Repositories.OrderRepository;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/order")
public class OrderController {
    private final SupplyRepository sup;
    private OrderRepository repo;
    private Sender send;
    private OrderTransaction orderTransaction;
    private ElementCartRepository elemRepo;
   // private ReentrantLock lock = new ReentrantLock();

    @Autowired
    public OrderController(OrderRepository repo, OrderTransaction orderTransaction, SupplyRepository sup,ElementCartRepository elemRepo,Sender s ) {
        this.repo = repo;
        this.send=s;
        this.elemRepo=elemRepo;
        this.orderTransaction=orderTransaction;
        this.sup=sup;
    }

    @RequestMapping(value = "/{userId}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ShopOrder> findAllOrder(long userId){
        //System.out.println("ECCOMI " + userId);
        return repo.findByUserId(String.valueOf(userId));
    }

    @RequestMapping(value = "/shop/{shopId}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<ShopOrder> findAllOrderForShop(long shopId){
        return repo.findByShopId(shopId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> createOrder(@RequestBody ObjectNode Body) throws JSONException, JsonProcessingException, InterruptedException {
        JSONObject risposta= new JSONObject();
        ElementCart el=new ElementCart();
   /*     while (!isLockAcquired) {
             isLockAcquired = this.lock.tryLock(3, TimeUnit.SECONDS);
        }

    */
            try {
                //Ottengo Order
                ShopOrder o = new ObjectMapper().convertValue(Body.get("Order"), ShopOrder.class);
                o.setInsertDate(new Timestamp(new Date().getTime()));
                o = repo.save(o);
                //Ottengo gli Elem
                JsonNode items = Body.get("OrderItem");
                for (int i = 0; i < items.size(); i++) {
                    el = new ObjectMapper().convertValue(items.get(i), ElementCart.class);
                    //Creating the ObjectMapper object
                    List<ElementCart> result= elemRepo.findAllByOrderIdAndSupplyID(o.getId(),el.getSupplyID());
                 if(result.isEmpty()){
                   if (orderTransaction.SaveOrder(el,o.getId())){
                            String jsonString =  new ObjectMapper().writeValueAsString(sup.findById(el.getSupplyID()).get());
                            send.sendingMessage(jsonString);
                        } else {
                            throw new Exception("Item Non valido");
                        }
                    } else {
                        throw new Exception("Item Non valido");
                    }
                }
                risposta.put("message", "Update OK");
                return new ResponseEntity<>(risposta.toString(), HttpStatus.OK);
            } catch (Exception e) {
                if (e.toString().contains("Item Non valido")) {
                    ObjectMapper mapper = new ObjectMapper();
                    risposta.put("message", "Ordine non valido");
                    risposta.put("elementChart", mapper.writeValueAsString(el));
                    return new ResponseEntity<>(risposta.toString(), HttpStatus.BAD_REQUEST);
                } else {
                    e.printStackTrace();
                    return new ResponseEntity<>(e.toString(), HttpStatus.BAD_REQUEST);
                }
            } finally {
             //   this.lock.unlock();
            }
    }

    @RequestMapping(value="/updateStatus", method = RequestMethod.POST)
    public ShopOrder updateStatus(@RequestBody ObjectNode body){
        this.repo.updateStatus(body.get("id").asLong(), body.get("newStatus").asText());
        return this.repo.findById(body.get("id").asLong()).get();
    }

    @RequestMapping(value="/findDone", method = RequestMethod.POST)
    public List<ShopOrder> findOrdersDone(@RequestBody  String buyerId){
        return this.repo.findOdersDone(buyerId);
    }

    @RequestMapping(value="/findReceived", method = RequestMethod.POST)
    public List<ShopOrder> findOrdersReceived(@RequestBody  String sellerId){
        return this.repo.findOdersReceived(sellerId);
    }

    @RequestMapping(value = "/detail/{userId}",method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public List<OrderDetail> getOrderDetail(long userId){
        List<OrderDetail> detailList = new ArrayList<OrderDetail>();
        try{
            List<ShopOrder> orderList = repo.findByUserId(String.valueOf(userId));
            for (ShopOrder so: orderList) {
                List<ElementCart> elementList = elemRepo.findByOrderId(so.getId());
                int i = 0;
                for(ElementCart e: elementList){
                    Supply s = sup.findSupplyById(e.getSupplyID());
                    detailList.add(new OrderDetail(s.getProduct().getName(), s.getProduct().getCategory(), Double.valueOf(e.getQuantity()), s.getProduct().getType(),
                            so.getId(), so.getShopId(), so.getUserId()));
                    System.out.println(detailList.get(i).toString());
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }
        return detailList;
    }
}
