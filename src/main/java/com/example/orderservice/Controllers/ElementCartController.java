package com.example.orderservice.Controllers;

import com.example.orderservice.Entities.ElementCart;
import com.example.orderservice.RabbitMQ.Sender;
import com.example.orderservice.Repositories.ElementCartRepository;
import com.example.orderservice.Repositories.OrderRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/ElementCart")
public class ElementCartController {
    private ElementCartRepository repo;
    @Autowired
    public ElementCartController(ElementCartRepository repo) {
        this.repo = repo;
    }

//    @RequestMapping(method = RequestMethod.GET)
//    @ResponseStatus(HttpStatus.OK)
//    public List<ElementCart> findAllElementCart(long orderId){
//        return repo.findByOrderId(orderId);
//    }

    @RequestMapping(method = RequestMethod.POST)
    public void addElementCart(@RequestBody ElementCart product) {
        repo.save(product);
        try {
            //Creating the ObjectMapper object
            ObjectMapper mapper = new ObjectMapper();
            //Converting the Object to JSONString
            String jsonString = mapper.writeValueAsString(product);
            System.out.println(jsonString);
            // send.sendingMessage(jsonString);
        }catch(JsonProcessingException jpex){

        }
    }

    @RequestMapping(value="/findByOrderId", method = RequestMethod.POST)
    public List<ElementCart> findByOrderId(@RequestBody Long orderId) {
        return this.repo.findByOrderId(orderId);
    }


}
