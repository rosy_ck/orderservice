package com.example.orderservice.Repositories;

import com.example.orderservice.Entities.Product;
import com.example.orderservice.Entities.Supply;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
}
