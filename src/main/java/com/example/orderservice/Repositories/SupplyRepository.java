package com.example.orderservice.Repositories;

import com.example.orderservice.Entities.Supply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

public interface SupplyRepository extends JpaRepository<Supply, Long> {

    @Query("SELECT s FROM Supply s WHERE s.shopId = ?1")
    List<Supply> findByShop(Long shopid);

    @Query("SELECT s FROM Supply s WHERE s.id = ?1 ORDER BY s.id DESC")
    Supply findSupplyById(Long id);
}
