package com.example.orderservice.Repositories;


import com.example.orderservice.Entities.ElementCart;
import com.example.orderservice.Entities.ShopOrder;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import javax.transaction.Transactional;

public interface OrderRepositoryCustom  {
    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    boolean SaveOrder(ElementCart el,long orderId);
}
