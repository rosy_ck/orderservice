package com.example.orderservice.Repositories;


import com.example.orderservice.Entities.ShopOrder;
import com.example.orderservice.Entities.OrderDetail;
import com.example.orderservice.Entities.Supply;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.LockModeType;
import java.util.List;

public interface OrderRepository extends JpaRepository<ShopOrder, Long> {
    //List<ShopOrder> findByUserId(long userId);
    List<ShopOrder> findByUserId(String userId);

    List<ShopOrder> findByShopId(long shopId);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("UPDATE ShopOrder o set o.status = ?2 WHERE o.id = ?1")
    int updateStatus(long orderId, String newStatus);

    @Query("SELECT order FROM ShopOrder order WHERE order.userId = ?1")
    List<ShopOrder> findOdersDone(String buyerId);

    @Query("SELECT order FROM ShopOrder order WHERE order.shopId = ?1")
    List<ShopOrder> findOdersReceived(String shopId);
}
