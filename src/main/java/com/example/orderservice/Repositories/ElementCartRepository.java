package com.example.orderservice.Repositories;

import com.example.orderservice.Entities.ElementCart;
import com.example.orderservice.Entities.ShopOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ElementCartRepository extends JpaRepository<ElementCart, Long> {
    //@Query("SELECT supplyID,quantità FROM ElementCart  WHERE ElementCart.orderId = ?1")
    //List<ElementCart> findByOrderId(Long orderId);
   // @Query("SELECT supplyID,quantità FROM ElementCart  WHERE ElementCart.orderId = ?1 and ElementCart.supplyID=?2")
    List<ElementCart> findAllByOrderIdAndSupplyID(Long orderId,Long SupplyId);

    @Query("SELECT element FROM ElementCart element WHERE element.orderId = ?1")
    List<ElementCart> findByOrderId(Long orderId);
}
