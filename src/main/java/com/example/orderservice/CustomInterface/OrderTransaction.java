package com.example.orderservice.CustomInterface;

import com.example.orderservice.Entities.ElementCart;
import com.example.orderservice.Entities.ShopOrder;
import com.example.orderservice.Entities.Supply;
import com.example.orderservice.Repositories.ElementCartRepository;
import com.example.orderservice.Repositories.OrderRepositoryCustom;
import com.example.orderservice.Repositories.SupplyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class OrderTransaction implements OrderRepositoryCustom {
    private SupplyRepository supplyRepository;
    private ElementCartRepository elemRepo;

    public OrderTransaction(SupplyRepository supplyRepository, ElementCartRepository elemRepo) {
        this.supplyRepository = supplyRepository;
        this.elemRepo = elemRepo;
    }

    @Override
    public boolean SaveOrder(ElementCart el, long orderId) {
      // ObjectMapper mapper = new ObjectMapper();
        Long supplyId = el.getSupplyID();
        Double quantity = Double.parseDouble(el.getQuantity());
        el.setOrderId(orderId);
        Optional<Supply> sup = supplyRepository.findById(supplyId);
        //find by two vlaues
       // List<ElementCart> result= elemRepo.findAllByOrderIdAndSupplyID(orderId,el.getSupplyID());
        if (sup.isPresent() ) {
            elemRepo.save(el);
            Supply su = sup.get();
            Double newQuantity = su.getQuantity() - quantity;
            if (newQuantity >= 0) {
                su.setQuantity(newQuantity);
                supplyRepository.save(su);
                return true;
            }else {
                elemRepo.delete(el);
                return false;
            }
    }else{
        return false;
        }
    }
}
