package com.example.orderservice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.JsonNode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@IdClass(CompositeKey.class)
public class ElementCart {
    @Id
    private long orderId;
    @Id
    private long supplyID;
    private String quantity;
//vedere se aggiungiere il prezzo


    @Override
    public String toString() {
        return "ElementCart{" +
                "orderId=" + orderId +
                ", supplyID=" + supplyID +
                ", quantità='" + quantity + '\'' +
                '}';
    }


    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
}


