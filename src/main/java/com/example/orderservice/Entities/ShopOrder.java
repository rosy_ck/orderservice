package com.example.orderservice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class ShopOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String shopId;
    private String userId;
    private String status;
    private Boolean payed;
    private String paymentType;
    private Timestamp insertDate;
    private float totalPrice;

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    @Override
    public String toString() {
        return "ShopOrder{" +
                "id=" + id +
                ", shopId='" + shopId + '\'' +
                ", userId='" + userId + '\'' +
                ", status='" + status + '\'' +
                ", payed=" + payed +
                ", payment_type='" + paymentType + '\'' +
                ", insert_date=" + insertDate +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
