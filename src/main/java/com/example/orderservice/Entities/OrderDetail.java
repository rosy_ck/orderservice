package com.example.orderservice.Entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class OrderDetail {
    private String productName;
    private String productCategory;
    private Double productQuantity;
    private String productType;
    private Long orderId;
    private String shopId;
    private String userId;

    public OrderDetail(String productName, String productCategory, Double productQuantity, String productType, Long orderId, String shopId, String userId) {
        this.productName = productName;
        this.productCategory = productCategory;
        this.productQuantity = productQuantity;
        this.productType = productType;
        this.orderId = orderId;
        this.shopId = shopId;
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "OrderDetail{" +
                "productName='" + productName + '\'' +
                ", productCategory='" + productCategory + '\'' +
                ", productQuantity=" + productQuantity +
                ", productType='" + productType + '\'' +
                ", orderId=" + orderId +
                ", shopId='" + shopId + '\'' +
                ", userId='" + userId + '\'' +
                '}';
    }
}
