package com.example.orderservice.Entities;

import java.io.Serializable;

public class CompositeKey implements Serializable {
    private long orderId;
    private long supplyID;

    public CompositeKey() {}

    public CompositeKey(long orderId, long supplyID) {
        this.orderId = orderId;
        this.supplyID = supplyID;
    }
}
