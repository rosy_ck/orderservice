package com.example.orderservice.RabbitMQ;


import com.example.orderservice.Entities.Product;
import com.example.orderservice.Entities.Supply;
import com.example.orderservice.Repositories.ProductRepository;
import com.example.orderservice.Repositories.SupplyRepository;
import com.fasterxml.jackson.core.JsonProcessingException;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import java.util.concurrent.CountDownLatch;

@Component
public class Reader {
    private final SupplyRepository supplyRepository;
    private final ProductRepository productRepository;
    private CountDownLatch latch = new CountDownLatch(1);

    Reader(SupplyRepository supplyRepository, ProductRepository productRepository){
        this.supplyRepository=supplyRepository;
        this.productRepository = productRepository;
    }

    public void readMessage(String message) throws JsonProcessingException {
        try {
            System.out.println("Received by Object Receiver <" + message + ">");
            //conversione in oggetto Supply
            ObjectMapper om = new ObjectMapper();
            Supply newproduct = om.readValue(message, Supply.class);
            //Recupero il prodotto con la get()
            Product p = newproduct.getProduct();
            //se il prodotto è già presente non lo inserisco e aggiungo solo la sua supply
            if(!productRepository.existsById(p.getId())){
                productRepository.save(p);
                System.out.println("Add new Product " + p.getName() + " with id " +p.getId() + " " );
            }
            System.out.println(newproduct.toString());
            supplyRepository.save(newproduct);
            System.out.println("New product and quantity correctly added");
            latch.countDown();
        }catch(Exception ex){
            System.out.println("EXCEPTION occurred during msg Reader: " + ex.getStackTrace());
        }
    }

    public CountDownLatch getLatch() {
        return latch;
    }
}
