package com.example.orderservice.RabbitMQ;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//Classe di configurazione : tutte le classi con questo tag vengono eseguite già dall'inizio

@Configuration
public class RabbitManager {

    //@Value("${spring.rabbitmq.queue}")
    private static final String queue = "orderMQ";

    //@Value("${spring.rabbitmq.exchange}")
    private String exchange = "tofoodExchange";

    //@Value("${spring.rabbitmq.routingkey}")
    private String routingKey = "order.#";

    //@Value("${spring.rabbitmq.username}")
    private String username = "guest";

    //@Value("${spring.rabbitmq.password}")
    private String password = "guest";

    //@Value("${spring.rabbitmq.host}")
    private String host = "host.docker.internal";
    //private String host = "localhost";

    @Bean
    Queue queue() {
        // return new Queue(queue);
        return new Queue(queue, true);
    }

    @Bean
    Exchange myExchange() {
        return ExchangeBuilder.topicExchange(exchange).durable(true).build();
    }

    @Bean
    Binding binding() {
        return BindingBuilder
                .bind(queue())
                .to(myExchange())
                .with(routingKey)
                .noargs();
    }

    @Bean
    public ConnectionFactory connectionFactory() {
        System.out.println("You are here");
        CachingConnectionFactory cachingConnectionFactory = new CachingConnectionFactory(host);
        cachingConnectionFactory.setUsername(username);
        cachingConnectionFactory.setPassword(password);
        return cachingConnectionFactory;
    }

    @Bean
    public ObjectMapper objectMapper() { return new ObjectMapper(); }

    @Bean
    public MessageConverter jsonMessageConverter() {
        // return new ObjectMapper().convertValue(objectNode.get("Shop"), Shop.class);
        return new Jackson2JsonMessageConverter(objectMapper());
    }

    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        System.out.println("Rabbit reader");
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
//container e ilstener servono per definire un Listener che resta sempre in ascolto
// per vedere se ci sono nuovi emssaggi
//dentro la coda
    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory());
        container.setQueueNames(queue);
        //listenerAdapter.setMessageConverter(jsonMessageConverter());
        container.setMessageListener(listenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(Reader reader) {
//        QueueingConsumer consumer = new QueueingConsumer(channel);
//        channel.basicConsume(QUEUE_NAME, true, consumer);
//        while (true) {
//            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
//            String message = new String(delivery.getBody());
//            System.out.println(" [x] Received '" + message + "'");
//        }
        //metodo che permette la lettura continua dei messaggi
        return new MessageListenerAdapter(reader, "readMessage");
    }
}
