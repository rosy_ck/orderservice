package com.example.orderservice.RabbitMQ;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Component
public class Sender {
    //sender della rabbitMQ
    private final RabbitTemplate rabbitTemplate;
    //private final Receiver receiver;

    //Receiver receiver, RabbitTemplate rabbitTemplate
    public Sender() {
        //this.receiver = new Receiver();
        //in questo punto sender si connettte alla connection factory adatta
        this.rabbitTemplate = new RabbitTemplate(RabbitSenderManager.connectionFactory());
    }

    public int sendingMessage(String jsonMessage) {
        try {
            rabbitTemplate.convertAndSend(RabbitSenderManager.topicExchangeName, "supply.qt.#", jsonMessage);
            //receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
            System.out.println("Msg mandato");
            return 0;
        } catch(Exception ex){
            return -1;
        }
    }
}
