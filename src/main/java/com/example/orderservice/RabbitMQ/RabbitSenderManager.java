package com.example.orderservice.RabbitMQ;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.context.annotation.Bean;

public class RabbitSenderManager {
    //nome dell'exchange (posso averne più di uno in un server rabbit)
    public static final String topicExchangeName = "tofoodExchange";

    static final String queueName = "supplyquantityMQ"; //nome della coda da qui passano i messagi, all'interno dell exchange

    //qua viene creata effettivamente la coda
    // durable:true i messaggi non vengono eliminati,se non vengono consumati
    @Bean
    Queue queue() {
        return new Queue(queueName, true);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(topicExchangeName);
    }

    @Bean
    Binding binding(Queue queue, TopicExchange exchange) {
        //la routing key serve per riconoscere dove instradare il messaggio
        return BindingBuilder.bind(queue).to(exchange).with("supply.qt.#");
    }

    @Bean
    public static ConnectionFactory connectionFactory() {
        //qua ci colleghiamo alla coda
        CachingConnectionFactory connectionFactory = new CachingConnectionFactory("host.docker.internal");
        //CachingConnectionFactory connectionFactory = new CachingConnectionFactory("localhost");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        System.out.println("Manager Sender: " + connectionFactory.getUsername());
        return connectionFactory;//stringa di connessione
    }
}
