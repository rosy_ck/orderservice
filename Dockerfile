FROM openjdk:15-alpine
RUN addgroup -S spring && adduser -S spring -G spring
VOLUME /tmp
EXPOSE 8086
ARG DEPENDENCY=target
ADD ${DEPENDENCY}/*.jar orderservice-2.4.4.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/orderservice-2.4.4.jar"]
CMD ["orderserviceapp", "serve", "--host", "0.0.0.0"]
